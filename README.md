# obnam-benchmark -- tool to run Obnam benchmarks

[Obnam][] is an encrypting backup program. `obnam-benchmark` runs
performance tests with Obnam: it generates various kinds of test data,
and runs Obnam to make backups, and restore them, and finally writes
out a file with measurements for the various parts of the process.

`obnam-benchmark` can also read a set of such measurement files and
produce a report with all results, and a summary table.

The Obnam project collects [result files][] in a git repository, and
builds a [report page][] whenever a new result is added.

See [obnam-benchmark.md][] for more details and examples.


[Obnam]: https://obnam.org/
[result files]: https://gitlab.com/obnam/obnam-benchmark-results
[report page]: https://doc.obnam.org/obnam-benchmark-results/
[obnam-benchmark.md]: obnam-benchmark.md


## Legalese


Copyright 2021-2022  Lars Wirzenius and others

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
