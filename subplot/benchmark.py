import json
import os


def install_rust_program(ctx):
    runcmd_prepend_to_path = globals()["runcmd_prepend_to_path"]
    srcdir = globals()["srcdir"]

    # Add the directory with built Rust binaries to the path.
    default_target = os.path.join(srcdir, "target")
    target = os.environ.get("CARGO_TARGET_DIR", default_target)
    runcmd_prepend_to_path(ctx, dirname=os.path.join(target, "debug"))


def file_is_at_least_this_long(ctx, filename=None, number=None):
    st = os.lstat(filename)
    assert st.st_size >= int(number)


def json_files_match(ctx, first=None, second=None):
    assert_eq = globals()["assert_eq"]
    first = json.load(open(first))
    second = json.load(open(second))
    assert_eq(first, second)


def file_is_valid_json(ctx, filename=None):
    json.load(open(filename))
