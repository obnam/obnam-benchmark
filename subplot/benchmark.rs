// Implementations of Subplot scenario steps for sshca.md.

use serde_json::value::Value;
use std::fs::File;
use std::path::Path;
use subplotlib::steplibrary::datadir::Datadir;
use subplotlib::steplibrary::runcmd::Runcmd;

#[derive(Default)]
struct SubplotContext {}

impl ContextElement for SubplotContext {}

#[step]
#[context(SubplotContext)]
#[context(Runcmd)]
fn install_rust_program(context: &ScenarioContext) {
    let target_exe = env!("CARGO_BIN_EXE_obnam-benchmark");
    let target_path = Path::new(target_exe);
    let target_path = target_path.parent().ok_or("No parent?")?;
    context.with_mut(
        |context: &mut Runcmd| {
            context.prepend_to_path(target_path);
            Ok(())
        },
        false,
    )?;
}

#[step]
#[context(Datadir)]
#[context(SubplotContext)]
fn json_files_match(context: &ScenarioContext, first: &str, second: &str) {
    context.with(
        |context: &Datadir| {
            let first = context
                .canonicalise_filename(&first)
                .expect("can't use first filename");
            let first = read_json_file(&first)?;

            let second = context
                .canonicalise_filename(&second)
                .expect("can't use second filename");
            let second = read_json_file(&second)?;

            if first != second {
                eprintln!("JSON files differ:\n{:#?}\n\n{:#?}\n", first, second,);
                panic!("ERROR: JSON files differ");
            };

            Ok(())
        },
        false,
    )?;
}

#[step]
#[context(Datadir)]
#[context(SubplotContext)]
fn file_is_valid_json(context: &ScenarioContext, filename: &str) {
    context.with(
        |context: &Datadir| {
            let filename = context.canonicalise_filename(&filename)?;
            read_json_file(&filename)?;
            Ok(())
        },
        false,
    )?;
}

fn read_json_file(filename: &Path) -> anyhow::Result<Value> {
    let file = File::open(filename)?;
    Ok(serde_json::from_reader(&file)?)
}

#[step]
#[context(Datadir)]
#[context(SubplotContext)]
fn file_is_at_least_this_long(context: &ScenarioContext, filename: &str, number: u64) {
    context.with(
        |context: &Datadir| {
            let filename = context.canonicalise_filename(&filename)?;
            let meta = std::fs::metadata(&filename)?;
            if meta.len() < number {
                panic!("file is {} bytes, wanted at least {} bytes", meta.len(), number);
            }
            Ok(())
        },
        false,
    )?;
}
