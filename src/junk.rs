use rand::RngCore;
use std::fs::File;
use std::io::Write;

const BUFSIZ: usize = 4096;

pub fn junk(f: &mut File, n: u64) -> Result<(), std::io::Error> {
    let mut remain = n;
    while remain >= BUFSIZ as u64 {
        write_junk(f, BUFSIZ)?;
        remain -= BUFSIZ as u64;
    }
    write_junk(f, remain as usize)?;
    Ok(())
}

fn write_junk(f: &mut File, n: usize) -> Result<(), std::io::Error> {
    let mut bytes = [0u8; BUFSIZ];
    rand::thread_rng().fill_bytes(&mut bytes);
    f.write_all(&bytes[..n])?;
    Ok(())
}
