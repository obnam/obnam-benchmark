//! Run benchmarks for the Obnam backup program.
//!
//! Obnam is a backup system. This crate a way to specifying
//! benchmarks in YAML and running them. A specification file looks
//! like:
//!
//! ```yaml
//! benchmarks:
//!   - benchmark: maildir
//!     backups:
//!       - changes:
//!           - create:
//!               files: 100
//!               file_size: 0
//!       - changes:
//!           - rename:
//!               files: 10
//!           - delete:
//!               files: 10
//!           - create:
//!               files: 10
//!               file_size: 0
//! ```
//!
//! A [specification][] is broken down into a sequence of
//! [steps](step). Steps, if executed in order, perform the benchmark.
//!
//! This crate only collects data from a set of benchmarks. It does
//! not analyze the data. The data can be stored for later analysis.

pub mod builder;
pub mod client;
pub mod daemon;
pub mod junk;
pub mod obnam;
pub mod report;
pub mod result;
pub mod server;
pub mod specification;
pub mod step;
pub mod suite;
pub mod summain;
pub mod tlsgen;
