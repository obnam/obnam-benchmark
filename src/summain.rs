use std::path::Path;
use std::process::Command;

#[derive(Debug, thiserror::Error)]
pub enum SummainError {
    #[error("failed to run summain: {0}")]
    Run(std::io::Error),
}

pub fn summain(root: &Path) -> Result<String, SummainError> {
    let output = Command::new("summain")
        .arg(".")
        .current_dir(root)
        .output()
        .map_err(SummainError::Run)?;
    if output.status.code() != Some(0) {
        eprintln!("{}", String::from_utf8_lossy(&output.stdout));
        eprintln!("{}", String::from_utf8_lossy(&output.stderr));
        std::process::exit(1);
    }

    Ok(String::from_utf8_lossy(&output.stdout).to_string())
}
