use crate::specification::{Change, Create, FileCount};

/// A step specification in the execution of a benchmark.
#[derive(Debug)]
pub enum Step {
    /// Start a benchmark.
    Start(
        /// Unique name of the benchmark.
        String,
    ),
    /// Finish a benchmark with a given name.
    Stop(
        /// Unique name of the benchmark.
        String,
    ),
    /// Create test data files.
    Create(Create),
    /// Rename test data files.
    Rename(FileCount),
    /// Delete test data files.
    Delete(FileCount),
    /// Make the nth backup in the benchmark.
    Backup(
        /// n
        usize,
    ),
    /// Restore the nth backup in the benchmark.
    Restore(
        /// n
        usize,
    ),
    /// Create and remember a manifest of current live data. Call it id.
    ManifestLive(
        /// id
        usize,
    ),
    /// Create and remember a manifest of latest restored data. Call it id.
    ManifestRestored(
        /// id
        usize,
    ),
    /// Compare two manifests for equality.
    CompareManifests(
        /// First
        usize,
        /// Second.
        usize,
    ),
}

/// Possible errors from executing a benchmark step.
#[derive(Debug, thiserror::Error)]
pub enum StepError {
    /// Generic I/O error.
    #[error(transparent)]
    Io(std::io::Error),
}

impl Step {
    pub(crate) fn from(change: &Change) -> Self {
        match change {
            Change::Create(x) => Self::Create(x.clone()),
            Change::Rename(x) => Self::Rename(x.clone()),
            Change::Delete(x) => Self::Delete(x.clone()),
        }
    }
}
