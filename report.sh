#!/bin/bash

set -euo pipefail

obnam-benchmark report "$@" |
	pandoc -f markdown+pipe_tables -t html -H obnam-benchmark.css \
		--metadata title="Obnam benchmarks" - -o /dev/stdout
